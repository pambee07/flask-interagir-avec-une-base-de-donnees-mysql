# cars_controller.py
from flask import Blueprint, jsonify, request
from app import mysql

cars_controller = Blueprint('cars', __name__)


@cars_controller.route('/', methods=['GET'])
def get_car_list():
    # Crée un curseur pour exécuter des requêtes SQL
    cursor = mysql.connection.cursor()
    # Sélectionne tous les voitures de la table car
    cursor.execute('SELECT * FROM car')
    # Récupère tous les résultats de la requête
    cars = cursor.fetchall()
    # Convertit les résultats en JSON et les renvoie
    return jsonify(cars)

# création d'une voiture
@cars_controller.route('/', methods=['POST'])
def add_car():
    # Récupère les données envoyées en JSON
    data = request.get_json()
    # Extrait la marque du JSON
    brand = data['brand']
    # Extrait le model du JSON
    model = data['model']
    cursor = mysql.connection.cursor()
    # Insère les données dans la base de données
    cursor.execute('INSERT INTO car (brand, model) VALUES (%s, %s)', (brand, model))
    # Applique les modifications
    mysql.connection.commit()
    return 'Voiture ajoutée', 201

# mise à jour d'une voiture
@cars_controller.route('/<int:id>', methods=['PUT'])
def update_car(id):
    data = request.get_json()
    brand = data['brand']
    model = data['model']
    cursor = mysql.connection.cursor()
    # Met à jour les informations de la voiture
    cursor.execute('UPDATE car SET brand = %s, model = %s WHERE car_id = %s', (brand, model, id))
    mysql.connection.commit()
    return 'Voiture mise à jour', 200

# suppression d'une voiture
@cars_controller.route('/<int:id>', methods=['DELETE'])
def delete_car(id):
    cursor = mysql.connection.cursor()
    # Supprime la voiture de la base de données
    cursor.execute('DELETE FROM car WHERE car_id = %s', (id,))
    mysql.connection.commit()
    return 'Voiture supprimée', 200

# Bonne pratique - requête préparée
@cars_controller.route('/<string:brand>', methods=['GET'])
def get_car(brand):
    cursor = mysql.connection.cursor()
    # Utilise une requête préparée, plus sécurisée
    cursor.execute('SELECT * FROM car WHERE brand = %s', (brand,))
    car = cursor.fetchone()
    if car:
        return jsonify(car)
    else:
        return 'voiture non trouvée', 404