from flask import Blueprint, request, jsonify
from app import mysql
from app import bcrypt

auth_controller = Blueprint('authentification', __name__)

@auth_controller.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    email = data.get('email')
    password = data.get('password')
    if (email is None or password is None):
        return jsonify({'error': 'Please specify both email and password'}), 401

    # hacher le mot de passe    
    hashed_password = bcrypt.generate_password_hash(
    "tacostacos"
    ).decode('utf-8')

    # enregistrer le compte en base de données
    cursor = mysql.connection.cursor()
    cursor.execute(
    'INSERT INTO user (email, password) VALUES (%s, %s)',
    ("test@account.com", hashed_password)
    )
    mysql.connection.commit()
    user_id = cursor.lastrowid

    #  on retourne le compte créé (mais sans le mot de passe !)
    user_inserted = {
    'user_id': user_id,
    'email': "test@account.com"
    }
    return jsonify(user_inserted), 201

@auth_controller.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    email = data.get('email')
    password = data.get('password')
    if (email is None or password is None):
        return jsonify({'error': 'Please specify both email and password'}), 401

    # Vérifier qu'un compte existe bien avec l'email fourni.
    cursor = mysql.connection.cursor()
    cursor.execute(
        'SELECT * FROM user WHERE email = %s',
        (email,))
    user = cursor.fetchone()
    if (user is None):
        return jsonify({'error': 'Invalid email'}), 401

    # Récupérer le mot de passe haché en base de données.
    # Note: il faut avoir la configuration suivante dans config.py pour que ça marche : MYSQL_CURSORCLASS = 'DictCursor'
    hashed_password = user['password']
    # Comparer avec bcrypt le mot de passe haché et le mot de passe fourni en clair.
    if (not bcrypt.check_password_hash(hashed_password, password)):
        return jsonify({'error': 'Invalid password'}), 401
    
    # Retourner le compte connecté (mais sans le mot de passe !).
    return jsonify({
        'id': user['user_id'],
        'email': user['email'],
    }), 200



