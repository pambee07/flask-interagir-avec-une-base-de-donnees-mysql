from flask import Flask
from flask_mysqldb import MySQL
from flask_cors import CORS
from dotenv import load_dotenv
from flask_bcrypt import Bcrypt

load_dotenv()

app = Flask(__name__)

bcrypt = Bcrypt(app)

CORS(app, resources={r"/*": {"origins": "http://localhost:8080"}})

app.config.from_object('config')

# Crée un objet MySQL pour interagir avec la base de données
mysql = MySQL(app)

# importe le middleware
from logger_middleware import log_request
# initialise le middleware pour logger les requêtes
app.before_request(log_request)


# importe les blueprints une fois la base de données initialisée
from cars_controller import cars_controller
from garages_controller import garages_controller
from authentification_controller import auth_controller
app.register_blueprint(cars_controller, url_prefix='/cars')
app.register_blueprint(garages_controller, url_prefix='/garages')
app.register_blueprint(auth_controller, url_prefix='/auth')






