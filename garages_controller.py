# garages_controller.py
from flask import Blueprint, jsonify, request
from validator_middleware import validate_garage_data
from app import mysql

garages_controller = Blueprint('garages', __name__)


@garages_controller.route('/', methods=['GET'])
def get_garage_list():
    # Crée un curseur pour exécuter des requêtes SQL
    cursor = mysql.connection.cursor()
    # Sélectionne tous les garages de la table car
    cursor.execute('SELECT * FROM garage')
    # Récupère tous les résultats de la requête
    garages = cursor.fetchall()
    # Convertit les résultats en JSON et les renvoie
    return jsonify(garages)

# création d'un garage
@garages_controller.route('/', methods=['POST'])
@validate_garage_data
def add_garage():
    # Récupère les données envoyées en JSON
    data = request.get_json()
    # Extrait la marque du JSON
    name = data['name']
    # Extrait le model du JSON
    email = data['email']
    cursor = mysql.connection.cursor()
    # Insère les données dans la base de données
    cursor.execute('INSERT INTO garage (name, email) VALUES (%s, %s)', (name, email))
    # Applique les modifications
    mysql.connection.commit()
    return 'Garage ajouté', 201

# mise à jour d'un garage
@garages_controller.route('/<int:id>', methods=['PUT'])
@validate_garage_data
def update_garage(id):
    data = request.get_json()
    name = data['name']
    email = data['email']
    cursor = mysql.connection.cursor()
    # Met à jour les informations du garage
    cursor.execute('UPDATE garage SET name = %s, email = %s WHERE garage_id = %s', (name, email, id))
    mysql.connection.commit()
    return 'Garage mis à jour', 200

# suppression d'un garage
@garages_controller.route('/<int:id>', methods=['DELETE'])
def delete_garage(id):
    cursor = mysql.connection.cursor()
    # Supprime le garage de la base de données
    cursor.execute('DELETE FROM garage WHERE garage_id = %s', (id,))
    mysql.connection.commit()
    return 'Garage supprimé', 200

# Bonne pratique - requête préparée
@garages_controller.route('/<string:brand>', methods=['GET'])
def get_garage(name):
    cursor = mysql.connection.cursor()
    # Utilise une requête préparée, plus sécurisée
    cursor.execute('SELECT * FROM garage WHERE name = %s', (name))
    garage = cursor.fetchone()
    if garage:
        return jsonify(garage)
    else:
        return 'Garage non trouvé', 404