# validator_middleware.py
import re
from flask import jsonify, request
from functools import wraps

def validate_garage_data(next):
    @wraps(next)
    def wrapper(*args, **kwargs):
        data = request.json
        name = data.get('name')
        email = data.get('email')

        # Vérifier que le nom et le mail sont présents dans la requête
        if not name or not email:
            return jsonify({'message': 'Le nom et le mail sont obligatoires.'}), 400
        
         # Vérifier le format de l'email avec une regex
        email_pattern = re.compile(r'^[\w\.-]+@[\w\.-]+\.\w+$')
        if not email_pattern.match(email):
            return jsonify({'message': 'Format d\'email invalide.'}), 400

        else:
            # passe au middleware suivant
            return next(*args, **kwargs)

    return wrapper

def validate_car_data(next):
    @wraps(next)
    def wrapper(*args, **kwargs):
        data = request.json
        brand = data.get('brand')
        model = data.get('model')

        # Vérifier que la marque et le model sont présents dans la requête
        if not brand or not model:
            return jsonify({'message': 'La marque et le model sont obligatoires.'}), 400
        else:
            # passe au middleware suivant
            return next(*args, **kwargs)

    return wrapper
